//Palindrom

const arrP = [7, 6, 1, 1, 6, 5, 5, 7, 5];
const arrF = [7, 6, 1];

const result = [
    ...arrF,
    ...arrP.filter(check => !arrF.includes(check))
    ]
    
console.log(`${result.join('')}${arrF.reverse().join('')}`);

//Associative-Array
arr = [
    {id: 2, value: 'row1', position: 'S'},
    {id: 2, value: 'row2', position: 'A'},
    {id: 1, value: 'row3', position: 'S'},
    {id: 3, value: 'row4', position: 'S'},
    {id: 1, value: 'row5', position: 'A'},
    {id: 4, value: 'row6', position: 'A'},
    {id: 5, value: 'row7', position: 'S'},
    {id: 5, value: 'row8', position: 'A'},
    ]

    arr.sort((a, b) => a.id - b.id);
    
    ret = arr.reduce((prev, curr) => {
        if (!prev[curr.id - 1]) {
             prev[curr.id - 1] = ['', ''];
        }
        
        if (curr.position === 'A') {
            prev[curr.id - 1][1] = curr.value;
        } else {
            prev[curr.id - 1][0] = curr.value;
        }
        
        return prev;
    }, {});
    
    console.log(Object.values(ret));
    
    
    /* 
    Note Associative-Array
    Untuk case ini saya tidak murni mengerjakan sendiri, saya coba open diskusi
    di stackoverflow untuk part masalah yang belum bisa saya selesaikan.
    */