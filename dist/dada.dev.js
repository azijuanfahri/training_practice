"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//Palindrom
var arrP = [7, 6, 1, 1, 6, 5, 5, 7, 5];
var arrF = [7, 6, 1];
var result = [].concat(arrF, _toConsumableArray(arrP.filter(function (check) {
  return !arrF.includes(check);
})));
console.log("".concat(result.join('')).concat(arrF.reverse().join(''))); //Associative-Array

arr = [{
  id: 2,
  value: 'row1',
  position: 'S'
}, {
  id: 2,
  value: 'row2',
  position: 'A'
}, {
  id: 1,
  value: 'row3',
  position: 'S'
}, {
  id: 3,
  value: 'row4',
  position: 'S'
}, {
  id: 1,
  value: 'row5',
  position: 'A'
}, {
  id: 4,
  value: 'row6',
  position: 'A'
}, {
  id: 5,
  value: 'row7',
  position: 'S'
}, {
  id: 5,
  value: 'row8',
  position: 'A'
}];
arr.sort(function (a, b) {
  return a.id - b.id;
});
ret = arr.reduce(function (prev, curr) {
  if (!prev[curr.id - 1]) {
    prev[curr.id - 1] = ['', ''];
  }

  if (curr.position === 'A') {
    prev[curr.id - 1][1] = curr.value;
  } else {
    prev[curr.id - 1][0] = curr.value;
  }

  return prev;
}, {});
console.log(Object.values(ret));
/* 
Note Associative-Array
Untuk case ini saya tidak murni mengerjakan sendiri, saya coba open diskusi
di stackoverflow untuk part masalah yang belum bisa saya selesaikan.
*/