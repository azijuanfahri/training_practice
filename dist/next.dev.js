"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

// const arr = [
//     {id: 2, value: 'row1', position: 'S'},
//     {id: 2, value: 'row2', position: 'A'},
//     {id: 1, value: 'row3', position: 'S'},
//     {id: 3, value: 'row4', position: 'S'},
//     {id: 1, value: 'row5', position: 'A'},
//     {id: 4, value: 'row6', position: 'A'},
//     {id: 5, value: 'row7', position: 'S'},
//     {id: 5, value: 'row8', position: 'A'}
//   ]
// // const groupData = a.reduce((acc, post) => {
// //   let {id, value} = post;
// //   return {...acc,[id]: [...(acc[id] || []), value]};
// // }, {});
// // console.log(groupData);
// const groupData = arr.reduce((acc, post) => {
//   const {id, value} = post;
//     return {...acc, [id]: [...(acc[id] || [] ) , value]};
// }, {});
// console.log(groupData);
// // const arr = [
// //   { "value": "abc", "checked": true },
// //   { "value": "xyz", "checked": false },
// //   { "value": "lmn", "checked": true }
// // ]
// const groupBy = (arr, key) => arr.reduce(
//   (result, item) => ({
//     ...result,
//     [item[key]]: [
//       ...(result[item[key]] || []),
//       item,
//     ],
//   }), 
//   {},
// );
// console.log(groupBy);
// arr = [
//   {id: 2, value: 'row1', position: 'S'},
//   {id: 2, value: 'row2', position: 'A'},
//   {id: 1, value: 'row3', position: 'S'},
//   {id: 3, value: 'row4', position: 'S'},
//   {id: 1, value: 'row5', position: 'A'},
//   {id: 4, value: 'row6', position: 'A'},
//   {id: 5, value: 'row7', position: 'S'},
//   {id: 5, value: 'row8', position: 'A'}
// ]
// arr.sort((a,b) => a.id - b.id);
// ret = arr.reduce((prev, curr) => {
// if(!prev[curr.id - 1]) {
//   prev[curr.id - 1] = ['', ''];
// }
// if(curr.position === 'A') {
//     prev[curr.id - 1][1] = curr.value;
//   } else {
//     prev[curr.id - 1][0] = curr.value;
//   }
// return prev;
// }, {});
// console.log(Object.values(ret));
//Associative-Array
arr = [{
  id: 2,
  value: "row1",
  position: "S"
}, {
  id: 2,
  value: "row2",
  position: "A"
}, {
  id: 1,
  value: "row3",
  position: "S"
}, {
  id: 3,
  value: "row4",
  position: "S"
}, {
  id: 1,
  value: "row5",
  position: "A"
}, {
  id: 4,
  value: "row6",
  position: "A"
}, {
  id: 5,
  value: "row7",
  position: "S"
}, {
  id: 5,
  value: "row8",
  position: "A"
}];
arr.sort(function (a, b) {
  return a.id - b.id;
});
ret = arr.reduce(function (prev, curr) {
  if (!prev[curr.id - 1]) {
    prev[curr.id - 1] = ['', ''];
  }

  if (curr.position === 'A') {
    prev[curr.id - 1][1] = curr.value;
  } else {
    prev[curr.id - 1][0] = curr.value;
  }

  return prev;
}, {});
console.log(Object.values(ret));

var patternLeft = function patternLeft(n) {
  for (var i = 1; i <= n; i++) {
    var str = '';

    for (var j = 1; j <= i; j++) {
      if (j % 3 === 0) {
        str += '%';
      } else if (j % 3 === 2) {
        str += '#';
      } else if (j % 3 === 1) {
        str += '*';
      }
    }

    console.log(str);
  }
};

patternLeft(7); // soal 2

var arrP = [7, 6, 1, 1, 6, 5, 5, 7, 5];
var arrFilter = [7, 6, 1];
var result = [].concat(arrFilter, _toConsumableArray(arrP.filter(function (check) {
  return !arrFilter.includes(check);
})));
console.log("".concat(result.join('')).concat(arrFilter.reverse().join(''))); //  var results = [];
//  for(var i=0; i<arr.length-1; i++) {
//    if(arr[i].id === arr[i].id){
//      results.push(arr[i].value)
//    }
//  }
//  console.log(results);